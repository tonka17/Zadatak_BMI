package com.example.antonija.v14_z1;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener {

    private Button bCalculateBMI;
    private TextView tvResult, tvResultExplanation;
    private EditText etWeightInput, etHeightInput;
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initializeUI();
    }

    private void initializeUI()
    {
        this.bCalculateBMI= (Button) this.findViewById(R.id.bCalculateBMI);
        this.bCalculateBMI.setOnClickListener(this);
        this.tvResult= (TextView) this.findViewById(R.id.tvResult);
        this.tvResultExplanation= (TextView) this.findViewById(R.id.tvResultExplanation);
        this.etHeightInput=(EditText) this.findViewById(R.id.etHeightInput);
        this.etWeightInput=(EditText) this.findViewById(R.id.etWeightInput);
        this.image=(ImageView) this.findViewById(R.id.image);
    }

    @Override
    public void onClick(View view)
    {
        String height_s = this.etHeightInput.getText().toString();
        String weight_s = this.etWeightInput.getText().toString();
        double height = Double.parseDouble(height_s);
        double weight = Double.parseDouble(weight_s);
        if(height>2.5||height<=0)
        {
            this.displayToast(getString(R.string.toastHeight));
            this.tvResult.setText(String.valueOf(0));
        }
        else if(weight>350||weight<=0)
        {
            this.displayToast(getString(R.string.toastWeight));
            this.tvResult.setText(String.valueOf(0));
        }
        else if(height<2.5||height>0&&weight<350||weight>0) {
            double height_square = height * height;
            double BMI = weight / height_square;
            this.tvResult.setText(String.format("%.2f", BMI));
            if (BMI < 18.5 && BMI > 0) {
                this.tvResultExplanation.setText(R.string.Underweight);
                this.image.setImageResource(R.drawable.underweight);
            } else if (BMI >= 18.5 && BMI < 24.9) {
                this.tvResultExplanation.setText(R.string.Normal);
                this.image.setImageResource(R.drawable.normal);
            } else if (BMI >= 25 && BMI < 29.9) {
                this.tvResultExplanation.setText(R.string.Overweight);
                this.image.setImageResource(R.drawable.overweight);
            } else if (BMI >= 30) {
                this.tvResultExplanation.setText(R.string.Obese);
                this.image.setImageResource(R.drawable.obese);
            }
        }
    }
    private void displayToast(String s)
    {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }
}
